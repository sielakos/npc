import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { DetailsComponent } from './details/details.component';
import { LoginModule } from '../login/login.module';
import { LoginGuardGuard } from '../login/login-guard.guard';

const routes: Routes = [
  {
    path: '',
    component: DetailsComponent,
    canActivate: [LoginGuardGuard],
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes), LoginModule],
  exports: [RouterModule],
})
export class DetailsRoutingModule {}
