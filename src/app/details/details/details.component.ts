import { Component, OnInit } from '@angular/core';
import { SocialAuthService, SocialUser } from 'angularx-social-login';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

@Component({
  selector: 'app-details',
  templateUrl: './details.component.html',
  styleUrls: ['./details.component.scss'],
})
export class DetailsComponent implements OnInit {
  private user$: Observable<SocialUser>;
  public name$: Observable<string>;
  public email$: Observable<string>;
  public photo$: Observable<string>;

  constructor(private authService: SocialAuthService) {
    this.user$ = authService.authState;

    this.name$ = map((user: SocialUser) => user.name)(this.user$);
    this.email$ = map((user: SocialUser) => user.email)(this.user$);
    this.photo$ = map((user: SocialUser) => user.photoUrl)(this.user$);
  }

  ngOnInit(): void {}
}
