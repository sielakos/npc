import { Injectable } from '@angular/core';
import {
  CanActivate,
  ActivatedRouteSnapshot,
  RouterStateSnapshot,
  UrlTree,
  Router,
} from '@angular/router';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { Select } from '@ngxs/store';

import { LoginState, LoginStateModel } from './login.state';

@Injectable({
  providedIn: 'root',
})
export class LoginGuardGuard implements CanActivate {
  @Select(LoginState) loginState$: Observable<LoginStateModel>;

  constructor(private router: Router) {}

  canActivate(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot
  ):
    | Observable<boolean | UrlTree>
    | Promise<boolean | UrlTree>
    | boolean
    | UrlTree {
    return map((state: LoginStateModel) => {
      if (state.loggedIn) {
        return true;
      }

      this.router.navigate(['/login']);

      return false;
    })(this.loginState$);
  }
}
