import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Select, Store } from '@ngxs/store';
import { SocialAuthService, GoogleLoginProvider } from 'angularx-social-login';
import { Observable } from 'rxjs';

import { LoginState, LoginStateModel } from '../login.state';
import { LoggedAction } from '../login.action';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss'],
})
export class LoginComponent implements OnInit {
  @Select(LoginState) login$: Observable<LoginStateModel>;

  constructor(
    private router: Router,
    private authService: SocialAuthService,
    private store: Store
  ) {}

  ngOnInit() {
    this.login$.subscribe((state) => {
      console.log(state);

      if (state.loggedIn) {
        this.router.navigate(['/']);
      }
    });

    this.authService.authState.subscribe((user) => {
      console.log(user);

      if (user) {
        this.store.dispatch(new LoggedAction());
      }
    });
  }

  onLogin() {
    this.authService.signIn(GoogleLoginProvider.PROVIDER_ID);
  }
}
