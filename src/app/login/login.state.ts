import { Injectable } from '@angular/core';
import { State, Action, StateContext } from '@ngxs/store';

import { LoggedAction } from './login.action';

export interface LoginStateModel {
  loggedIn: boolean;
}

@State<LoginStateModel>({
  name: 'animals',
  defaults: {
    loggedIn: false,
  },
})
@Injectable()
export class LoginState {
  @Action(LoggedAction)
  logIn(ctx: StateContext<LoginStateModel>) {
    const state = ctx.getState();

    ctx.setState({
      ...state,
      loggedIn: true,
    });
  }
}
